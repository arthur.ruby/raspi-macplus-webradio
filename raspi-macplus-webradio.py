from raspi_macplus_webradio.application import Application
from raspi_macplus_webradio.models import load_json_file
from raspi_macplus_webradio.constants import RADIO_STATIONS_FILE

load_json_file(RADIO_STATIONS_FILE)
app = Application()
app.mainloop()
