from unittest import TestCase, mock

from raspi_macplus_webradio import models
from raspi_macplus_webradio.application import Application


class AppVolumeTest(TestCase):
    def setUp(self):
        # Edith updates the webradio list and adds three sations.
        self.mock_open = mock.mock_open(
            read_data=('{"title":"Radio Uno", "address":"http://www.radio.es/radio-uno.mp3"}\n'
                       '{"title":"Zwei Radio", "address":"http://deutschland.radio.de/zwei.mp3"}\n'
                       '{"title":"Radio-Trois", "address":"http://super.radiofrance.fr/radiotrois.mp3"}'
                       )
        )
        self.mock_stations = []
        with mock.patch('raspi_macplus_webradio.models.open', self.mock_open), \
             mock.patch('raspi_macplus_webradio.models.stations', self.mock_stations):
            models.load_json_file('filename')

            # She lauches the app.
            self.app = Application()
        self.app.wait_visibility()

    def test_volume_handling_in_app(self):
        # A window opens. She can read the name of the app.
        self.assertEqual(self.app.wm_title(), 'Mac Plus Radio')
        self.assertEqual(self.app.view.inside_window.top_bar.label.cget('text'), 'Mac Plus Radio')

        # Edith can see that the counter list displays three stations she entered.
        self.assertEqual(self.app.view.inside_window.navigation.station_counter_label.cget('text'), '1/3')

        # The first webradio from the list is played. She can read its title.
        self.assertEqual(self.app.view.inside_window.navigation.station_display_label.cget('text'), 'Radio Uno')

        # The volume is set to 50% by default.
        volume = self.app.view.inside_window.volume
        self.assertEqual(volume.scale_label.cget('text'), 'Volume: 50%')

        # It's quite not enought for Edith. She increases it by clicking five times on the right side of the cursor.
        x_pos = volume.scale.winfo_width() - 5
        y_pos = volume.scale.winfo_height() / 2
        self.click_on_widget(widget=volume.scale, x=x_pos, y=y_pos, times=5)
        # 55% is way better!
        self.assertEqual(volume.scale_label.cget('text'), 'Volume: 55%')

        # Or is it? Actually not. She puts it back to 50% by clicking five times on the left side of the cursor.
        x_pos = 5
        self.click_on_widget( widget=volume.scale, x=x_pos, y=y_pos, times=5 )
        self.assertEqual(volume.scale_label.cget('text'), 'Volume: 50%')

        # Someone calls her on the telephone. She slides the cursor all the way to the left.
        mid_x_pos = (volume.scale.winfo_width() / 2)
        self.click_and_drag_slider(widget=volume.scale, press_x_pos=mid_x_pos, release_x_pos=x_pos)

        # Now the volume is muted at 0%, she can have her chat.
        self.assertEqual(volume.scale_label.cget('text'), 'Volume: 0%')

    def tearDown(self):
        # Well, there's no need to keep it running if nobody's listening. She turns it off.
        self.mock_stations.clear()
        self.app.update()
        self.app.destroy()

    def click_on_widget(self, widget, x, y, times=1, button=1):
        for _ in range(times):
            widget.focus_force()
            self.app.update()
            widget.event_generate(f'<ButtonPress-{button}>', x=x, y=y)
            self.app.update()

    def click_and_drag_slider(self, widget, press_x_pos, release_x_pos):
        widget.focus_force()
        self.app.update()
        widget.event_generate(f'<ButtonPress-1>', x=press_x_pos, y=widget.winfo_height() / 2)
        self.app.update()
        widget.event_generate(f'<Motion>', x=release_x_pos, y=widget.winfo_height() / 2)
        self.app.update()
        widget.event_generate(f'<ButtonRelease-1>')
        self.app.update()