from unittest import TestCase, mock

from raspi_macplus_webradio import models
from raspi_macplus_webradio.application import Application


class AppNavigationTest(TestCase):
    def setUp(self):
        # Edith updates the webradio list and adds three sations.
        self.mock_open = mock.mock_open(
            read_data=('{"title":"Radio Uno", "address":"http://www.radio.es/radio-uno.mp3"}\n'
                       '{"title":"Zwei Radio", "address":"http://deutschland.radio.de/zwei.mp3"}\n'
                       '{"title":"Radio-Trois", "address":"http://super.radiofrance.fr/radiotrois.mp3"}'
                       )
        )
        self.mock_stations = []
        with mock.patch('raspi_macplus_webradio.models.open', self.mock_open), \
             mock.patch('raspi_macplus_webradio.models.stations', self.mock_stations):
            models.load_json_file('filename')

            # She lauches the app.
            self.app = Application()
        self.app.wait_visibility()

    def test_can_navigate_between_radio_stations(self):
        # A window opens. She can read the name of the app.
        self.assertEqual(self.app.wm_title(), 'Mac Plus Radio')
        self.assertEqual(self.app.view.inside_window.top_bar.label.cget('text'), 'Mac Plus Radio')

        # Edith can see that the counter list displays three stations she entered.
        self.assertEqual(self.app.view.inside_window.navigation.station_counter_label.cget('text'), '1/3')

        # The first webradio from the list is played. She can read its title.
        self.assertEqual(self.app.view.inside_window.navigation.station_display_label.cget('text'), 'Radio Uno')

        # She presses the "next" button once.
        self.app.view.inside_window.navigation.next_button.invoke()
        # self.simple_clic_and_release_on_button(button=self.app.view.inside_window.navigation.next_button, times=1)

        # The next radio station's title is displayed.
        self.assertEqual(self.app.view.inside_window.navigation.station_display_label.cget('text'), 'Zwei Radio')

        # The radio stations counter also displayes the new station number.
        self.assertEqual(self.app.view.inside_window.navigation.station_counter_label.cget('text'), '2/3')

        # The radio station, obviously, is also played.

        # She now presses two times on the "next" button.
        for _ in range(2):
            self.app.view.inside_window.navigation.next_button.invoke()
        # self.simple_clic_and_release_on_button(button=self.app.view.inside_window.navigation.next_button, times=2)

        # She sees that the list "wrapped" back to the first radio station.
        self.assertEqual(self.app.view.inside_window.navigation.station_counter_label.cget('text'), '1/3')
        self.assertEqual(self.app.view.inside_window.navigation.station_display_label.cget('text'), 'Radio Uno')

        # Edith presses the "previous" button juste once.
        self.app.view.inside_window.navigation.previous_button.invoke()
        # self.simple_clic_and_release_on_button(button=self.app.view.inside_window.navigation.previous_button, times=1)

        # Now the list wrapped the other way around: the app plays and displays the last radio sation.
        self.assertEqual(self.app.view.inside_window.navigation.station_counter_label.cget('text'), '3/3')
        self.assertEqual(self.app.view.inside_window.navigation.station_display_label.cget('text'), 'Radio-Trois')

    def tearDown(self):
        # She's quite happy with all of this and goes back to sleep.
        self.mock_stations.clear()
        self.app.update()
        self.app.destroy()

    # For some obscur reason I can't get the event_generate() method to work
    # so instead I have to call invoke() on the buttons.
    # def simple_clic_and_release_on_button(self, button, times):
    #     for _ in range(times):
    #         button.focus_force()
    #         button.event_generate('<ButtonPress-1>')
    #         print('button pressed')
    #         self.app.update()
    #         button.event_generate('<ButtonRelease-1>')
    #         print('button released')
    #         self.app.update()
