import json
import re

from .constants import ADDRESS_ERROR_MESSAGE

stations = []
errors = []


class AddressException(Exception):
    message = ADDRESS_ERROR_MESSAGE


class Station:
    def __init__(self, title, address):
        self.title = title
        self.address = address

    def _get_address(self):
        return self._address

    def _set_address(self, address):
        regex_pattern = r"^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$"
        regex_match = re.search(regex_pattern, address)
        if regex_match is None:
            raise AddressException
        self._address = address

    address = property(_get_address, _set_address)


def load_json_file(filename):
    with open(filename, mode="r", encoding="utf-8") as f:
        for station_json in f:
            try:
                station_dict = json.loads(station_json)
                station = Station(**station_dict)
                stations.append(station)
            except AddressException as ae:
                errors.append(f'{ae.message} on line: {station_json.strip()} - This line has been skipped.')
            except (TypeError, ValueError, KeyError):
                errors.append(
                    f'Decoding JSON has failed on the line: {station_json.strip()} - This line has been skipped.'
                )


def get_all_stations():
    return stations


def get_errors():
    return errors
