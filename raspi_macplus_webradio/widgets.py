import tkinter as tk

from . import constants, models


def create_horizontal_lines_set(canvas, y, lines):
    for _ in range(lines):
        canvas.create_line(2, y, 13, y)
        canvas.create_line(28, y, constants.WINDOW_TOP_BAR_WIDTH - 2, y)
        y += 2


class TaskBar(tk.Frame):
    def __init__(self, parent, *args, **kwargs):
        super().__init__(parent, *args, **kwargs)
        self.name = 'Taskbar'
        self.height = constants.TASKBAR_HEIGHT
        self.width = constants.APP_WIDTH
        self.bg = 'white'
        self.grid(column=0, row=0, columnspan=2, sticky=tk.W + tk.E)

        self.text = tk.StringVar(self, value=constants.TASKBAR_TEXT)

        self.canvas = tk.Canvas(self, bd=-2, bg='white', height=self.height, width=self.width)
        self.line = self.canvas.create_line(0, self.height, self.width, self.height, width=2)
        self.canvas.grid(columnspan=2, column=0, row=0)

        self.image_file = tk.PhotoImage(file=constants.APPLE_IMAGE_FILE)
        self.label = tk.Label(
            self, anchor=tk.W, bg='white', fg='black', font=('ChicagoFLF', 12), textvariable=self.text,
            compound=tk.LEFT, image=self.image_file, padx=4, pady=0
        )
        self.label.grid(column=0, row=0, sticky=tk.W)


class WindowTopBar(tk.Frame):
    def __init__(self, parent, *args, **kwargs):
        super().__init__(parent, *args, **kwargs)
        self.name = 'Window Top Bar'
        self.height = constants.WINDOW_TOP_BAR_HEIGHT
        self.width = constants.WINDOW_TOP_BAR_WIDTH
        self.bg = 'white'

        self.canvas = tk.Canvas(self, bd=-2, bg='white', height=self.height, width=self.width)
        self.rectangle = self.canvas.create_rectangle(-1, -1, self.width, self.height - 1)
        create_horizontal_lines_set(self.canvas, y=4, lines=6)
        self.square = self.canvas.create_rectangle(15, 4, 25, 15)
        self.canvas.grid(column=0, row=0)

        self.text = tk.StringVar(self, value=constants.APP_TITLE)
        self.label = tk.Label(
            self, anchor=tk.CENTER, bg='white', fg='black', font=('ChicagoFLF', 9), textvariable=self.text,
            padx=4, pady=0
        )
        self.label.grid(column=0, row=0)


class NavigationWidget(tk.Frame):
    def __init__(self, parent, *args, **kwargs):
        super().__init__(parent, *args, **kwargs)
        self.name = 'Navigation Widget'
        self.height = constants.WINDOW_HEIGHT
        self.width = constants.WINDOW_WIDTH
        self['bg'] = 'white'

        self.previous_button = tk.Button(
            self, anchor=tk.W, text=constants.BUTTON_PREVIOUS, font=('Impact', 20), justify=tk.CENTER, padx=20,
            command=lambda direction="prev": self.change_station(direction=direction)
        )
        self.previous_button.grid(column=0, row=0, sticky=tk.W)

        self.next_button = tk.Button(
            self, anchor=tk.E, text=constants.BUTTON_NEXT, font=('Impact', 20), justify=tk.CENTER, padx=20,
            command=lambda direction="next": self.change_station(direction=direction)
        )
        self.next_button.grid(column=2, row=0, sticky=tk.E)

        self.stations = self.get_all_stations()
        if self.stations:
            self.current_station = self.stations[0]
            self.current_index = self.stations.index(self.current_station)
        self.station_counter_text = tk.StringVar(value=f'{self.current_index + 1}/{len(self.stations)}')
        self.station_counter_label = tk.Label(
            self, anchor=tk.CENTER, bg='white', fg='black', font=('ChicagoFLF', 12), padx=4, pady=0, width=6,
            textvariable=self.station_counter_text
        )
        self.station_counter_label.grid(column=1, row=0, sticky=tk.W + tk.E)

        self.station_diplay_text = tk.StringVar(value=self.current_station.title)
        self.station_display_label = tk.Label(
            self, anchor=tk.CENTER, bg='white', fg='black', font=('ChicagoFLF', 26), padx=0, pady=2,
            textvariable=self.station_diplay_text
        )
        self.station_display_label.grid(column=0, row=1, columnspan=3)

    def get_all_stations(self):
        return models.get_all_stations()

    def change_station(self, direction):
        if direction == "next":
            if self.current_index < len(self.stations) - 1:
                self.current_index += 1
            else:
                self.current_index = 0
        elif direction == "prev":
            if self.current_index > 0:
                self.current_index -= 1
            else:
                self.current_index = len(self.stations) - 1
        self.current_station = self.stations[self.current_index]
        self.station_counter_text.set(f'{self.current_index + 1}/{len(self.stations)}')
        self.station_diplay_text.set(self.current_station.title)


class VolumeWidget(tk.Frame):
    def __init__(self, parent, *args, **kwargs):
        super().__init__(parent, *args, **kwargs)
        self.name = 'Volume Widget'
        self.height = constants.WINDOW_HEIGHT
        self.width = constants.WINDOW_WIDTH
        self['bg'] = 'white'

        self.scale_value = tk.IntVar(value=constants.VOLUME_DEFAULT_VALUE)
        self.scale_label = tk.Label(
            self, anchor=tk.CENTER, bg='white', fg='black', font=('ChicagoFLF', 9), padx=0, pady=4,
            text=f'{constants.VOLUME_LABEL}{self.scale_value.get()}%'
        )
        self.scale_label.grid(column=0, row=0)

        self.scale = tk.Scale(
            self, from_=0, to=100, font=('ChicagoFLF', 9), orient=tk.HORIZONTAL, length=self.width - 2, showvalue=0,
            variable=self.scale_value, command=self.update_scale_value
        )
        self.scale.grid(column=0, row=1)

    def update_scale_value(self, value):
        self.scale_value.set(value=value)
        self.scale_label.configure(text=f'{constants.VOLUME_LABEL}{self.scale_value.get()}%')
