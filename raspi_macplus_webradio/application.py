import platform
import tkinter as tk

from . import constants
from .views import MainView


class Application(tk.Tk):
    """Mac Plus Radio Main Application"""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.title(constants.APP_TITLE)
        self.geometry(f"{constants.APP_WIDTH}x{constants.APP_HEIGHT}")
        self['bg'] = '#808080'
        self.resizable(width=False, height=False)
        if platform.system() == 'Linux':
            self.attributes('-zoomed', True)
        self.view = MainView(self)
