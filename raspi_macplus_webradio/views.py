import tkinter as tk

from . import constants
from .widgets import TaskBar, WindowTopBar, NavigationWidget, VolumeWidget


class Window(tk.Frame):
    def __init__(self, parent, *args, **kwargs):
        super().__init__(parent, *args, **kwargs)
        self.name = 'Window'
        self.height = constants.WINDOW_HEIGHT
        self.width = constants.WINDOW_WIDTH
        self['bg'] = 'white'
        self['highlightthickness'] = 1
        self['highlightbackground'] = 'black'

        self.grid(column=0, row=1, columnspan=2, pady=(10, 0))
        self.top_bar = WindowTopBar(self)
        self.top_bar.grid(column=0, row=0)
        self.navigation = NavigationWidget(self)
        self.navigation.grid(column=0, row=1)
        self.volume = VolumeWidget(self)
        self.volume.grid(column=0, row=2)


class MainView(tk.Frame):
    def __init__(self, parent, *args, **kwargs):
        super().__init__(parent, *args, **kwargs)
        self.grid(column=0, row=0, padx=(0, 0))
        self['bg'] = '#808080'
        self.taskbar = TaskBar(self)
        self.inside_window = Window(self)
