APP_TITLE = 'Mac Plus Radio'
APP_WIDTH = 320
APP_HEIGHT = 240

TASKBAR_TEXT = 'File  Edit  View  Special'
TASKBAR_HEIGHT = 25
APPLE_IMAGE_FILE = 'resources/apple_menu_icon.png'
RADIO_STATIONS_FILE = 'data/radio_stations.txt'
WINDOW_WIDTH = 300
WINDOW_HEIGHT = 205

WINDOW_TOP_BAR_WIDTH = 300
WINDOW_TOP_BAR_HEIGHT = 20

BUTTON_NEXT = '►►'
BUTTON_PREVIOUS = '◄◄'

VOLUME_LABEL = 'Volume: '

UNICODE_ERROR_MESSAGE = 'The file has invalid encoding. Only UTF-8 is supported.'
IO_ERROR_MESSAGE = 'The file does not exist.'
ADDRESS_ERROR_MESSAGE = 'Invalid URL address'

VOLUME_DEFAULT_VALUE = 50
