import tkinter as tk
from unittest import TestCase
from unittest.mock import patch

from raspi_macplus_webradio.models import Station
from raspi_macplus_webradio.widgets import NavigationWidget, VolumeWidget


class TkTestCase(TestCase):
    """A test case designed for Tkinter widgets and views"""
    root = None

    @classmethod
    def setUpClass(cls):
        cls.root = tk.Tk()
        cls.root.wait_visibility()

    @classmethod
    def tearDownClass(cls):
        cls.root.update()
        cls.root.destroy()


class TestNavigationWidget(TkTestCase):

    def setUp(self):
        self.mock_stations = [
            Station(title="Radio Uno", address="http://www.radio.es/radio-uno.mp3"),
            Station(title="Zwei Radio", address="http://deutschland.radio.de/zwei.mp3"),
            Station(title="Radio-Trois", address="http://super.radiofrance.fr/radiotrois.mp3")
        ]
        with patch('raspi_macplus_webradio.widgets.models.stations', self.mock_stations):
            self.navigation = NavigationWidget(self.root)

    def tearDown(self):
        self.navigation.destroy()

    def test_stations_counter_displays_current_station_number_and_stations_count(self):
        self.assertEqual(self.navigation.station_counter_label.cget('text'), '1/3')

    def test_current_station_title_is_displayed(self):
        self.assertEqual(self.navigation.station_display_label.cget('text'), self.mock_stations[0].title)

    def test_change_station_command_in_both_directions(self):
        self.assertEqual(self.navigation.station_counter_label.cget('text'), '1/3')
        self.assertEqual(self.navigation.station_display_label.cget('text'), self.mock_stations[0].title)
        self.navigation.change_station("next")
        self.assertEqual(self.navigation.station_counter_label.cget('text'), '2/3')
        self.assertEqual(self.navigation.station_display_label.cget('text'), self.mock_stations[1].title)
        self.navigation.change_station("prev")
        self.assertEqual(self.navigation.station_counter_label.cget('text'), '1/3')
        self.assertEqual(self.navigation.station_display_label.cget('text'), self.mock_stations[0].title)

    def test_change_station_command_wraps_around_list(self):
        self.assertEqual(self.navigation.station_counter_label.cget('text'), '1/3')
        self.assertEqual(self.navigation.station_display_label.cget('text'), self.mock_stations[0].title)
        self.navigation.change_station("prev")
        self.assertEqual(self.navigation.station_counter_label.cget('text'), '3/3')
        self.assertEqual(self.navigation.station_display_label.cget('text'), self.mock_stations[2].title)
        self.navigation.change_station("next")
        self.assertEqual(self.navigation.station_counter_label.cget('text'), '1/3')
        self.assertEqual(self.navigation.station_display_label.cget('text'), self.mock_stations[0].title)


class TestVolumeWidget(TkTestCase):
    def setUp(self):
        self.volume = VolumeWidget(self.root)

    def tearDown(self):
        self.volume.destroy()

    def test_volume_is_set_to_fifty_percent_by_default_at_startup(self):
        self.assertEqual(self.volume.scale_label.cget('text'), 'Volume: 50%')

    def test_scale_command_updates_volume_value(self):
        self.assertEqual(self.volume.scale_label.cget('text'), 'Volume: 50%')
        self.volume.update_scale_value(60)
        self.assertEqual(self.volume.scale_label.cget('text'), 'Volume: 60%')
