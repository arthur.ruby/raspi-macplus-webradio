# import platform
from unittest import TestCase, mock
from raspi_macplus_webradio.application import Application


class TestApplication(TestCase):
    def setUp(self):
        self.app = Application()
        self.app.wait_visibility()

    def tearDown(self):
        self.app.update()
        self.app.destroy()

    # def test_application_general_geometry(self):
    #     self.assertIn('320x240', self.app.winfo_geometry())
    #     self.assertEqual(self.app.wm_resizable(), (False, False))

    # def test_app_is_displayed_fullscreen_on_linux(self):
    #     with mock.patch('platform.system'):
    #         platform.system.return_value = 'Linux'
    #         test_app = Application()
    #         self.assertIn('-zoomed', test_app.wm_attributes())

