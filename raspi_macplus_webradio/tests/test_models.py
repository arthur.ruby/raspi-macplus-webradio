from unittest import TestCase, mock

from raspi_macplus_webradio import models
from raspi_macplus_webradio.models import Station


class TestJsonFileToModel(TestCase):
    def setUp(self):
        self.mock_open = mock.mock_open(
            read_data=('{"title":"Radio Uno", "address":"http://www.radio.es/radio-uno.mp3"}\n'
                       '{"title":"Zwei Radio", "address":"http://deutschland.radio.de/zwei.mp3"}\n'
                       '{"title":"Radio-Trois", "address":"http://super.radiofrance.fr/radiotrois.mp3"}'
                       )
        )
        self.invalid_key_mock_open = mock.mock_open(
            read_data=('{"invalid":"Radio Uno", "address":"http://www.radio.es/radio-uno.mp3"}\n'
                       '{"title":"Zwei Radio", "address":"http://deutschland.radio.de/zwei.mp3"}\n'
                       '{"title":"Radio-Trois", "address":"http://super.radiofrance.fr/radiotrois.mp3"}'
                       )
        )
        self.invalid_address_mock_open = mock.mock_open(
            read_data=('{"title":"Radio Uno", "address":"invalid"}\n'
                       '{"title":"Zwei Radio", "address":"http://deutschland.radio.de/zwei.mp3"}\n'
                       '{"title":"Radio-Trois", "address":"http://super.radiofrance.fr/radiotrois.mp3"}'
                       )
        )
        self.invalid_json_mock_open = mock.mock_open(
            read_data=('{"invalid"}\n'
                       '{"title":"Zwei Radio", "address":"http://deutschland.radio.de/zwei.mp3"}\n'
                       '{"title":"Radio-Trois", "address":"http://super.radiofrance.fr/radiotrois.mp3"}'
                       )
        )
        self.error_messages_mock_open = mock.mock_open(
            read_data=('{"invalid"}\n'
                       '{"title":"Radio Uno", "address":"invalid"}\n'
                       '{"invalid":"Radio Uno", "address":"http://www.radio.es/radio-uno.mp3"}\n'
                       '{"title":"Radio Uno", "address":"http://www.radio.es/radio-uno.mp3"}\n'
                       '{"title":"Zwei Radio", "address":"http://deutschland.radio.de/zwei.mp3"}\n'
                       '{"title":"Radio-Trois", "address":"http://super.radiofrance.fr/radiotrois.mp3"}'
                       )
        )
        self.stations_mock = []
        self.errors_mock = []

    def tearDown(self):
        self.stations_mock.clear()
        self.errors_mock.clear()

    def test_create_models_from_json_data(self):
        with mock.patch('raspi_macplus_webradio.models.open', self.mock_open), \
             mock.patch('raspi_macplus_webradio.models.stations', self.stations_mock):
            models.load_json_file('filename')
            result = models.get_all_stations()
        expected_data = [Station(title="Radio Uno", address="http://www.radio.es/radio-uno.mp3"),
                         Station(title="Zwei Radio", address="http://deutschland.radio.de/zwei.mp3"),
                         Station(title="Radio-Trois", address="http://super.radiofrance.fr/radiotrois.mp3")]
        self.assertEqual((expected_data[0].title, expected_data[0].address), (result[0].title, result[0].address))
        self.assertEqual((expected_data[1].title, expected_data[1].address), (result[1].title, result[1].address))
        self.assertEqual((expected_data[2].title, expected_data[2].address), (result[2].title, result[2].address))

    def test_invalid_address_prevents_only_the_invalid_station_creation(self):
        with mock.patch('raspi_macplus_webradio.models.open', self.invalid_address_mock_open), \
             mock.patch('raspi_macplus_webradio.models.stations', self.stations_mock):
            models.load_json_file('filename')
            self.assertEqual(len(models.get_all_stations()), 2)

    def test_invalid_json_prevents_only_the_invalid_station_creation(self):
        with mock.patch('raspi_macplus_webradio.models.open', self.invalid_json_mock_open), \
             mock.patch('raspi_macplus_webradio.models.stations', self.stations_mock):
            models.load_json_file('filename')
            self.assertEqual(len(models.get_all_stations()), 2)

    def test_invalid_key_in_json_prevents_only_the_invalid_station_creation(self):
        with mock.patch('raspi_macplus_webradio.models.open', self.invalid_key_mock_open), \
             mock.patch('raspi_macplus_webradio.models.stations', self.stations_mock):
            models.load_json_file('filename')
            self.assertEqual(len(models.get_all_stations()), 2)

    def test_raised_errors_messages_are_appended_to_errors_list(self):
        with mock.patch('raspi_macplus_webradio.models.open', self.error_messages_mock_open), \
             mock.patch('raspi_macplus_webradio.models.stations', self.stations_mock), \
             mock.patch('raspi_macplus_webradio.models.errors', self.errors_mock):
            models.load_json_file('filename')
            self.assertEqual(len(models.get_all_stations()), 3)
            self.assertEqual(len(models.get_errors()), 3)

    def test_invalid_json_adds_correct_error_message_to_error_list(self):
        expected_message = 'Decoding JSON has failed on the line: {"invalid"} - This line has been skipped.'
        with mock.patch('raspi_macplus_webradio.models.open', self.invalid_json_mock_open), \
             mock.patch('raspi_macplus_webradio.models.stations', self.stations_mock), \
             mock.patch('raspi_macplus_webradio.models.errors', self.errors_mock):
            models.load_json_file('filename')
            error_messages = models.get_errors()
            self.assertEqual(len(models.get_all_stations()), 2)
            self.assertEqual(len(error_messages), 1)
            self.assertEqual(error_messages[0], expected_message)

    def test_invalid_address_adds_correct_error_message_to_error_list(self):
        expected_message = (
            'Invalid URL address on line: {"title":"Radio Uno", "address":"invalid"} - This line has been skipped.'
        )
        with mock.patch('raspi_macplus_webradio.models.open', self.invalid_address_mock_open), \
             mock.patch('raspi_macplus_webradio.models.stations', self.stations_mock), \
             mock.patch('raspi_macplus_webradio.models.errors', self.errors_mock):
            models.load_json_file('filename')
            error_messages = models.get_errors()
            self.assertEqual(len(models.get_all_stations()), 2)
            self.assertEqual(len(error_messages), 1)
            self.assertEqual(error_messages[0], expected_message)
